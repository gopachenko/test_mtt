<?php

namespace app\models;

use app\models\Country;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * CountrySearch represents the model behind the search form about `app\models\Country`.
 */

class CountrySearch extends ActiveRecord
{
 
    public function getCountriesAndCities() 
    {
        $countriesAndCities = ArrayHelper::map(Country::find()
            ->select('country.name as country_name, city.name as city_name')
            ->leftJoin('city', 'city.country_id = country.id')
            ->asArray()->all(), 'city_name', 'country_name');

        if (!$countriesAndCities) {
           return false; 
        }

        $result = false;
        foreach($countriesAndCities as $city => $country) {
            $result[$country][] = $city;
        }
        
        return $result;
    }
        
}