<?php

namespace app\models;

//use Yii;
//use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "city"
 * 
 * @property integer $id
 * @property string $name
 * @property integer $country_id
 */

class City extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country_id'], 'integer'],
            ['name', 'string', 'max' => 255],
        ];
    }
    
    /**
     * @inheritdoc  - можно выкинуть для логиkи не трeбyeтся
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'City Name',
            'country_id' => 'Country ID',
        ];
    }
}