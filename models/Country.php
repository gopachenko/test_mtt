<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "country"
 * 
 * @property integer $id
 * @property string $name
 */

class Country extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['name', 'string', 'max' => 255],
        ];
    }
    
    /**
     * @inheritdoc  - можно выкинуть для логиkи не трeбyeтся
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Country Name',
        ];
    }
    
}

