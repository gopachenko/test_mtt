<?php

namespace app\services\city;

use app\models\Country;
use app\models\City;
use yii\base\Exception;

/**
 * Class CityService
 * @package app\services\city
 */
class CityService
{
    /** @var City $city */
    public $city;

    public function __construct(City $city)
    {
        $this->city = $city;
    }
    
    /**
     * Create new city
     * @param string $cityName, $countryName
     * @return bool
     * @throws
     */
    public function create($cityName, $countryName) {
        $country = Country::findOne(['name' => $countryName]);
        if (!$country) {
            throw new Exception('Country not found or entered incorrectly');
        }
        $this->city->name = $cityName;
        $this->city->country_id = $country->id;
        
        if (!$this->city->validate()) {
            throw new Exception('Validation error, incorrect data entry');
        }
        if (!$this->city->save()) {
            throw new Exception('Can`t save city');
        }
        return true;
    }
    
    
    
}
