<?php

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\City;
use app\services\city\CityService;

/**
 * Class CityController
 * @package app\controllers
 */

class CityController extends Controller
{
     /**
     * This command create new city from an existing country
      * params: string $city, $country
      * @return bool
     */
    public function actionCreate($cityName, $countryName) {
        return (new CityService(new City()))->create($cityName, $countryName);
    }
}
