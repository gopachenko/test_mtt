<?php

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\CountrySearch;

/**
 * Class CountryController
 * @package app\controllers
 */

class CountryController extends Controller
{
    /**
     * This command came back counrties and cities list
     * @return null|array
     */
    public function actionIndex()
    {        
        return (new CountrySearch())->getCountriesAndCities();
    }
}

