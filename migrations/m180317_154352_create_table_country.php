<?php

use yii\db\Migration;

/**
 * Class m180317_154352_create_table_country
 */
class m180317_154352_create_table_country extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('country', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Name of Country'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('country');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180317_154352_create_table_country cannot be reverted.\n";

        return false;
    }
    */
}
