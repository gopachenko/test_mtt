<?php

use yii\db\Migration;

/**
 * Class m180317_154401_create_table_city
 */
class m180317_154401_create_table_city extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('city', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Name of City'),
            'country_id' => $this->integer()->comment('Foreign Key from Country Table'),
        ]);
        
        $this->addForeignKey('fk_city_country_id', 'city', 'country_id', 'country', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('city');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180317_154401_create_table_city cannot be reverted.\n";

        return false;
    }
    */
}
